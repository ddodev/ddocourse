// Build-Reveal

var Ddocourse = {
    create: function (_config) { // 'constructor'
        //  prevents direct instantiation
        //  but no inheritance
        return (function () {
            // private variable
            var config = _config || {

            }; // private variable

            // [some private functions]

            function getConfig() {
                return config;
            }

            function setConfig(_config) {
                config = _config;
            }

            return { // revealed functions
                getConfig: getConfig,
                setConfig: setConfig
            }
        })();
    }
}

 var p = Ddocourse.create({
            type: 'b'
        });

 // name will be set to 'defaultname'
//alert(p.getConfig().type); // alerts "adam:eva"
